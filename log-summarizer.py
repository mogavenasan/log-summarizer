import glob
import os
import re
import zipfile
import argparse
import sys
import urllib
import urllib2
from socket import getfqdn
import re

__author__ = 'Mogavenasan'

class summarizer(object):
    def __init__(self):
#Arguement parsing
        self.args = self.parse_arguments()
        self.support_zip = self.args.support_zip
        self.product = self.args.product

        self.submit_analytics(self.support_zip,self.product)

#Variables initialization
        self.support_zip_name = os.path.splitext(self.support_zip)[0]
        self.support_zip_name = os.path.basename(self.support_zip_name)
        self.abs_path = os.path.abspath(self.support_zip)
        self.dir_path = os.path.dirname(self.abs_path)
        self.extract_path = os.path.join(self.dir_path, self.support_zip_name)
        self.application_logs_path = os.path.join(self.extract_path, 'application-logs')
        self.log_sum_path = os.path.join(self.application_logs_path, 'log-summarizer')
        self.mergedfile_path = os.path.join(self.log_sum_path, 'mergedfile.txt')
        self.splitfile_path = os.path.join(self.log_sum_path, 'split-files')
        
        self.cat_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), self.support_zip)
        self.cat_folder_path = os.path.splitext(self.cat_path)[0]
        self.catsplitfile_path = os.path.join(self.cat_folder_path, 'split-files')

#Extract, merging and spliting files. ALL THE FREAKING MAGIC!
        if self.support_zip.endswith('.zip'):
            validate_zip_file = zipfile.is_zipfile(self.support_zip)
            if validate_zip_file:
                self.extract_zip(self.support_zip, self.extract_path)
                self.merge_files(self.product, self.application_logs_path, self.log_sum_path)
                self.split_files(self.product, self.mergedfile_path, self.splitfile_path)
                #Log analyzing for a summary.
                self.analyze_file(self.product, self.splitfile_path)
                print 'You can find the split files in %s.' %self.splitfile_path
                os.remove(self.mergedfile_path)
            else:
                print 'Invalid Support ZIP'
                sys.exit(1)
        elif self.support_zip.endswith(('.out', '.log')):
            os.mkdir(self.cat_folder_path)
            self.split_files(self.product, self.cat_path, self.catsplitfile_path)
            #Log analyzing for a summary.
            self.analyze_file(self.product, self.catsplitfile_path)
            print 'You can find the split files in %s.' %self.catsplitfile_path
        else:
            self.merge_files(self.product, self.application_logs_path, self.log_sum_path)
            self.split_files(self.product, self.mergedfile_path, self.splitfile_path)
            #Log analyzing for a summary.
            self.analyze_file(self.product, self.splitfile_path)
            print 'You can find the split files in %s.' %self.splitfile_path
            os.remove(self.mergedfile_path)

#Arguement parsing
    def parse_arguments(self):
        """
        Parses command-line arguments.
        """
        parser = argparse.ArgumentParser(description='Log summarizer.')
        parser.add_argument('support_zip', type=str, help='Support ZIP filename')
        parser.add_argument('product', type = str.lower, choices=['jira', 'confluence'], help='Atlassian Product')

        return self.check_arguments(parser.parse_args())

    def check_application_logs(self, args):
        atlassian_log = os.path.join(os.path.dirname(os.path.realpath(__file__)), args.support_zip ,'application-logs', 'atlassian-' + args.product + '.log')
                
        check_file_exist = os.path.exists(atlassian_log)

        if check_file_exist:
            pass
        else:
            print 'Wrong product or there is no application logs in the directory.'
            sys.exit(1)


    def check_arguments(self, args):
        """
        Check the arguments provided in parseArguments(). Makes some changes based ond defaults and so on.
        """
        if args.support_zip.endswith('.zip'):
            print 'You have provided a ZIP file.'
        elif args.support_zip.endswith(('.out', '.log')):
            print 'You have either provided a catalina.out file or atlassian-jira.log file.'
        else:
            print 'You have provided a directory.'
            self.check_application_logs(args)
            
        return args

#Extract, merging and spliting files.
    def extract_zip(self, support_zip, extract_path):
        """
        Extracts the content of the Support ZIP.
        """
        zip_ref = zipfile.ZipFile(self.support_zip, 'r')
        zip_ref.extractall(extract_path)
        zip_ref.close()
        print 'Extracting Support ZIP to %s.' %extract_path


    def merge_files(self, product, application_logs_path, log_sum_path):
        """
        Merge all the atlassian-jira.log* / atlassian-confluence.log* into one text file.
        """
        if product == 'jira':
            list_of_files = glob.glob(os.path.join(application_logs_path, 'atlassian-jira.log*'))
        elif product == "confluence":
            list_of_files = glob.glob(os.path.join(application_logs_path, 'atlassian-confluence.log*'))

        os.mkdir(log_sum_path)

        if not list_of_files:
            print 'Wrong product or there is no application logs in your Support ZIP.'
            sys.exit(1)

        with open(os.path.join(log_sum_path, 'mergedfile.txt'), 'w') as outfile:
            for fname in list_of_files:
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)

        print 'Merging log files.'

    def split_files(self, product, mergedfile_path, splitfile_path):
        """
        Split the merged text file to each jira/confluence startup.
        """
        os.mkdir(splitfile_path)
        #foogie's script!!
        if product == "jira":
            regexStarting = '.*JIRA\sstarting.*'
        elif product == "confluence":
            regexStarting = '.*Starting\sConfluence.*'

        f = open(mergedfile_path, 'r')
        i = 0

        for h in f:
            if re.match(regexStarting, h):
                i = i + 1
                if product == "confluence":
                    timestampdate = h[:10]
                    hour = h[11:13]
                    minute = h[14:16]
                    seconds = h[17:19]
                    timestamp = timestampdate + ' ' + hour + '-' + minute + '-' + seconds
                elif product == "jira":
                    f.next()
                    f.next()
                    h = f.next()
                    timestampdate = h[:10]
                    hour = h[11:13]
                    minute = h[14:16]
                    seconds = h[17:19]
                    timestamp = timestampdate + ' ' + hour + '-' + minute + '-' + seconds

                g = open(os.path.join(splitfile_path, timestamp + '.log'), 'w')
            if i > 0:
                if (g.closed != True):
                    g.write(h)
        if i > 0:
            g.close()
            f.close()

        print 'Spliting files.'

    def analyze_file(self, product, splitfile_path):
        """
        Analyzing the log files for a summary
        """
        startup_number = len(os.walk(splitfile_path).next()[2])
        if startup_number > 3:
            startup_number = 3
        else:
            startup_number = startup_number

        filename_list = []
        for filename in sorted(glob.glob(os.path.join(splitfile_path, '*.log')), reverse=True):
            filename = os.path.join(splitfile_path, os.path.basename(filename))
            filename_list.append(filename)

        m = 0
        latest_filename = []
        while m < startup_number:
            latest_filename.append(filename_list[m])
            m += 1

        #tchai's script!!!
        caps_product = product.upper()
        if product == "jira":
            find = ['%s Build' %caps_product,
                '    Base URL',
                '         Entity model schema name',
                '         Database Version',
                '         Database Driver',
                'Database URL',
                'Modified Files',
                'Removed Files',
                'user.name',
                '         %s Home' %caps_product,
                '         Current Working Directory',
                '         java.home'
                ]
        elif product == "confluence":
            find = ['version =',
                'baseUrl = ',
                'Database Dialect',
                'Database URL',
                'Database Driver Version',
                'Database Version',
                'userName =',
                'home =',
                'workingDirectory'
                ]

        for num, word in enumerate(find, start=1):
            array = []
            change = 0
            with open(os.path.join(splitfile_path, 'change.log'), 'a') as changefile:
                changeitem = '\n' + word.replace('=', '').lstrip().rstrip() + ":" + '\n'
                changefile.write(changeitem)
                for filename in latest_filename:
                    with open(filename) as f:
                        limit = 1
                        for line in f:
                            if limit != 1:
                                break
                            elif word in line:
                                array.append(line.lstrip())
                                limit += 1

                for i in xrange(0,len(array)):
                    if array[i] != array[0]:
                        change = 1
                if change == 1:
                    for a in xrange(0,len(array)):
                        if product == "jira":
                            changelog = '    ' + os.path.basename(latest_filename[a]) + ":" + array[a].split(':',1)[-1]
                        if product == "confluence":
                            changelog = '    ' + os.path.basename(latest_filename[a]) + ":" + array[a].replace(': ','= ',1).split('=',1)[-1]
                        changefile.write(changelog)
                if change == 0:
                    changelog = "    No Changes" + '\n'
                    changefile.write(changelog)

        if product == "jira":
            find = ['JVM Input Arguments']
        elif product == "confluence":
            find = ['jvmInputArguments']

        for num, word in enumerate(find, start=1):
            array = []
            change = 0
            with open(os.path.join(splitfile_path, 'change.log'), 'a') as changefile:
                changeitem = '\n' + word.lstrip() + ":" + '\n'
                changefile.write(changeitem)
                for filename in latest_filename:
                    with open(filename) as f:
                        for line in f:
                            if word in line:
                                array.append(line.lstrip())
                for i in xrange(0,len(array)):
                    array[i] = re.sub(r'(Xloggc).*[0-9].log[\s]', ' ',array[i])
                    if array[i] != array[0]:
                        change = 1
                if change == 1:
                    for a in xrange(0,len(array)):
                        if product == "jira":
                            changelog = '    ' + os.path.basename(latest_filename[a]) + ":" + array[a].split(':',1)[-1]
                        if product == "confluence":
                            changelog = '    ' + os.path.basename(latest_filename[a]) + ":" + array[a].split('=',1)[-1]
                        changefile.write(changelog)
                if change == 0:
                    changelog = "    No Changes" + '\n'
                    changefile.write(changelog)


    def submit_analytics(self, support_zip, product):
        """
        GA for tracking purpose.
        """
        version = 1
        tracking_id = 'UA-96089664-1'

        data = [{"v": version, "tid": tracking_id, "cid": getfqdn(), "t": "event", "ec": "arguments",
                "ea": "%s %s" % (self.support_zip,self.product)},]

        try:
            for i in range(0, len(data)):
                u = urllib2.urlopen("http://www.google-analytics.com/collect", urllib.urlencode(data[i]))
                u.close()
        except Exception as e:
            print("Failed to connect to Google Analytics:\n\t%s" % e)

if __name__ == '__main__':
    summarizer()
